﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            float number1 = float.Parse(textBox1.Text);
            float number2 = float.Parse(textBox2.Text);

            float result = number1 + number2;

            label2.Text = result.ToString();
        }
    }
}
